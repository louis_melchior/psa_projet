# Project of PSA
## Aim of this project
Write and use a 2D-FD solver for the time-dependent non-relativistic Schrödinger equation

## Small explaination
This project runs python2 and C++11 code together.

C++11 need :
```
Armadillo
```

We have unit test, made with :
```
cxxtest
```
