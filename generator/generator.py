#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# genere fichiers pour le solver
# maillage, step espace, step tps
# champs phi0 et champs V
# prévoir les options cmd pour générer les champs
#

import numpy

WIDTH = 0
LENGTH = 0
STITCH_SIZE = 0

def null_potential(x, y):
	return 0

def phi_zero(x, y):
	return numpy.exp(pow(-x,2))

class Mesh():
	def __init__(self, width = 0, length = 0, stitch_size = (0,0))
		self.width = max(0,width)
		self.length = max(0,lentgh)
		self.stitch_size = (max(0,stitch_size[0]), max(0,stitch_size[1])
		self.default_potential = null_potential
		
		if self.width <= 0: self.ask_width()
		if self.length <= 0: self.ask_length()
		if self.stitch_size[0] <= 0 or self.stitch_size[1] <= 0:
			self.ask_stitch_size()
	#endDef

	def ask_width(self):
	"""Asks the user for the width of the mesh."""
		new_width = 0
		while new_width <= 0:
			try:
				new_width = int(raw_input("Width of the mesh ? (in stitch, >0) "))
			except ValueError:
				print("Error : must be an integer.")
			#endTry
		#endWhile
		self.width = new_width
	#endDef
				
	def ask_length(self):
	"""Asks the user for the length of the mesh."""
		new_length = 0
		while new_length <= 0:
			try:
				new_length = int(raw_input("Length of the mesh ? (in stitch, >0) "))
			except ValueError:
				print("Error : must be an integer.")
			#endTry
		#endWhile
		self.length = new_length
	#endDef
				
	def ask_stich_size(self):
	"""Asks the user for the width of the mesh."""
		new_size = [0,0]
		while new_size[0] <= 0:
			try:
				new_size[0] = float(raw_input("Length of one stitch ? (fermi, >0) "))
			except ValueError:
				print("Error : must be an float.")
			#endTry
		#endWhile
		while new_size[1] <= 0:
			try:
				new_size[1] = float(raw_input("Width of one stitch ? (fermi, >0) "))
			except ValueError:
				print("Error : must be an float.")
			#endTry
		#endWhile
		self.stitch_size = tuple(new_size)
	#endDef
#endClass

class Field():
	def __init__(self, mesh, generate_function):
		self.mesh = mesh
		self.values
		self.generate_function = generate_function
		
		self.field_generator()		
	#endDef

	def field_generator(self):
		self.values = []
		for x in range(self.mesh.width):
			self.values.append([])
			for y in range(self.mesh.length):
				self.values[x].append(self.field_generator(x, y))
			#endFor
		#endFor
		self.values = numpy.array(self.values)
	#endDef
#endClass	

mesh = Mesh()
field_potential = Field(mesh, null_potential)
field_phi = Field(mesh, phi_zero)
