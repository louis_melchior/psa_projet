import numpy as np
from PIL import Image

# /!\ L'image suivante à été généré avec l'option teinte de gris /!\
img_name='haut_gauche.png'
img = Image.open(img_name)

M = np.array(img)   # M atrice numpy avec les valeurs

M.shape # Sort (Hauteur, Largeur)

# M[Hauteur][Largeur]

M[0][0] # Correspond à en haut à gauche

M[399][639] # En bas à droite

M[0][639]   # En haut à droite

print("0 correspond à noir, 255 à blanc")
